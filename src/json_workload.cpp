#include "json_workload.hpp"

#include <stdexcept>
#include <fstream>
#include <vector>
#include <limits>

#include <rapidjson/document.h>

#include "pempek_assert.hpp"

using namespace rapidjson;
using namespace std;

Workload::~Workload()
{
    deallocate();
}

Job * Workload::operator[](string jobID)
{
    PPK_ASSERT_ERROR(_jobs.count(jobID) == 1, "Job '%s' does not exist", jobID.c_str());
    return _jobs.at(jobID);
}

const Job * Workload::operator[](string jobID) const
{
    return _jobs.at(jobID);
}

void Workload::set_rjms_delay(Rational rjms_delay)
{
    PPK_ASSERT_ERROR(rjms_delay >= 0);
    _rjms_delay = rjms_delay;
}

void Workload::add_job_from_redis(RedisStorage & storage, const string &job_id, double submission_time)
{
    string job_json_desc_str = storage.get_job_json_string(job_id);
    PPK_ASSERT_ERROR(job_json_desc_str != "", "Cannot retrieve job '%s'", job_id.c_str());

    Job * job = job_from_json_description_string(job_json_desc_str);
    job->submission_time = submission_time;

    // Let's apply the RJMS delay on the job
    job->walltime += _rjms_delay;

    PPK_ASSERT_ERROR(_jobs.count(job_id) == 0, "Job '%s' already exists in the Workload", job_id.c_str());
    _jobs[job_id] = job;
}

void Workload::put_file_into_buffer(const string &filename)
{
    std::ifstream file(filename, std::ios::binary);
    if (!file.is_open())
        throw runtime_error("Cannot open file '" + filename + "'");

    file.seekg(0, std::ios::end);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    vector<char> buffer(size);
    if (!file.read(buffer.data(), size))
        throw runtime_error("Cannot read file '" + filename + "'");

    _fileContents = new char[size + 1];
    memcpy(_fileContents, buffer.data(), size);
    _fileContents[size] = '\0';
}

void Workload::deallocate()
{
    // Let's delete all allocated jobs
    for (auto& mit : _jobs)
    {
        delete mit.second;
    }

    _jobs.clear();

    delete _fileContents;
    _fileContents = nullptr;
}

Job* Workload::job_from_json_description_string(const string &json_string)
{
    Document document;

    if (document.Parse(json_string.c_str()).HasParseError())
        throw runtime_error("Invalid json string '" + json_string + "'");

    PPK_ASSERT_ERROR(document.HasMember("id"),
                     "Invalid json string '%s': no 'id' member",
                     json_string.c_str());
    PPK_ASSERT_ERROR(document["id"].IsString(),
                     "Invalid json string '%s': 'id' member is not a string",
                     json_string.c_str());
    PPK_ASSERT_ERROR(document.HasMember("walltime"),
                     "Invalid json string '%s': no 'walltime' member",
                     json_string.c_str());
    PPK_ASSERT_ERROR(document["walltime"].IsNumber(),
                     "Invalid json string '%s': 'walltime' member is not a number",
                     json_string.c_str());
    PPK_ASSERT_ERROR(document.HasMember("res"),
                     "Invalid json string '%s': no 'res' member",
                     json_string.c_str());
    PPK_ASSERT_ERROR(document["res"].IsInt(),
                     "Invalid json string '%s': 'res' member is not an integer",
                     json_string.c_str());

    Job * j = new Job;
    j->id = document["id"].GetString();
    j->walltime = document["walltime"].GetDouble();
    j->nb_requested_resources = document["res"].GetInt();
    j->unique_number = _job_number++;

    return j;
}
