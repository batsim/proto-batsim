base_output_directory: ${HOME}/expe/llh_waiting_time

base_variables:
    batsim_dir: ${HOME}/proj/batsim
    sched_dir: ${HOME}/proj/yasimbatch/cpp_scheduler

    base_sched_command: ${sched_dir}/build/scheduler
    platform_generator_script: ${batsim_dir}/platforms/energy_platform_homogeneous_no_net.py
    workload_generator_script: ${batsim_dir}/tools/swf_to_batsim_workload_compute_only.py

    agg_dir: ${base_output_directory}/aggregated
    platforms_dir: ${base_output_directory}/platforms
    workloads_dir : ${base_output_directory}/workloads

implicit_instances:
  implicit:
    sweep:
      workload :
        - {"name":"kth_sp2", "filename":"${base_output_directory}/workloads/kth_sp2.json", "platform_name":"homo100", "platform_file":"${base_output_directory}/platforms/homo100.xml"}
        - {"name":"sdsc_sp2", "filename":"${base_output_directory}/workloads/sdsc_sp2.json", "platform_name":"homo128", "platform_file":"${base_output_directory}/platforms/homo128.xml"}
        - {"name":"hpc2n", "filename":"${base_output_directory}/workloads/hpc2n.json", "platform_name":"homo240", "platform_file":"${base_output_directory}/platforms/homo240.xml"}
        - {"name":"intrepid", "filename":"${base_output_directory}/workloads/intrepid.json", "platform_name":"homo163840", "platform_file":"${base_output_directory}/platforms/homo163840.xml"}

        #- {"name":"medium", "filename":"${batsim_dir}/workload_profiles/batsim_paper_workload_example.json", "platform_name":"homo128", "platform_file":"${batsim_dir}/platforms/energy_platform_homogeneous_no_net_128.xml"}
        #- {"name":"curie", "filename":"${batsim_dir}/workload_profiles/cea_curie.json", "platform_name":"homo_curie", "platform_file":"${batsim_dir}/platforms/energy_platform_homogeneous_no_net_93312_curie.xml"}
      algo:
        - {"name":"easy_bf_plot", "sched_name":"easy_bf_plot_liquid_load_horizon"}

    generic_instance:
      timeout: 172800
      working_directory: ${base_working_directory}
      output_directory: ${base_output_directory}/results/${algo["name"]}__${workload["name"]}__${workload["platform_name"]}

      variables:
        instance_name: ${algo["name"]}__${workload["name"]}__${workload["platform_name"]}
        socket_filename: /tmp/batsock_${instance_name}

      batsim_command: batsim -p ${workload["platform_file"]} -w ${workload["filename"]} -E -e ${output_directory}/${instance_name} -s ${socket_filename} -L -T -q
      sched_command: ${base_sched_command} -v ${algo["sched_name"]} -s ${socket_filename} --variant_options_filepath ${output_directory}/sched_input.json 1>${output_directory}/_sched.stdout 2>${output_directory}/_sched.stderr

      commands_before_execution:
        - rm -f ${output_directory}/*.svg
        - |
            #!/bin/bash
            # Let's generate an input file for the scheduler
            cat > ${output_directory}/sched_input.json << EOF
            {
              "output_filename":"${output_directory}/sched_load_log.csv"
            }
            EOF

      commands_after_execution:
        - |
            #!/bin/bash
            cat > ${output_directory}/plotscript.R <<EOF
            library(ggplot2)
            library(TTR)

            j = read.csv("${output_directory}/${instance_name}_jobs.csv")
            l = read.csv("${output_directory}/sched_load_log.csv")

            pdf("${output_directory}/liquid.pdf", width=16, height=3)
            ggplot() +
            geom_point(data=j, aes(x=submission_time, y=waiting_time, color="Jobs' waiting times")) +
            geom_line(data=l, aes(x=date, y=liquid_load_horizon, color="Liquid load horizon"))
            dev.off()

            #j['ma100_waiting_time'] = SMA(j[["waiting_time"]], 100)
            l['ma100_liquid_load_horizon'] = SMA(l[["liquid_load_horizon"]], 100)
            pdf("${output_directory}/liquid_ma100.pdf", width=16, height=3)
            ggplot() +
            geom_point(data=j, aes(x=submission_time, y=waiting_time, color="Jobs' waiting times")) +
            geom_line(data=l, aes(x=date, y=ma100_liquid_load_horizon, color="Liquid load horizon (moving average over 100 values)"))
            dev.off()

            # Let's plot the same thing, but only for one month
            nb_secs_in_one_day = 60*60*24
            time_a = median(j[,"submission_time"])
            time_b = time_a + nb_secs_in_one_day * 7 * 4

            j2 = j[which(j[,"submission_time"] > time_a & j[,"submission_time"] < time_b),]
            l2 = l[which(l[,"date"] > time_a & l[,"date"] < time_b),]

            pdf("${output_directory}/liquid_one_month.pdf", width=16, height=3)
            ggplot() +
            geom_point(data=j2, aes(x=submission_time, y=waiting_time, color="Jobs' waiting times")) +
            geom_line(data=l2, aes(x=date, y=liquid_load_horizon, color="Liquid load horizon"))
            dev.off()

            pdf("${output_directory}/liquid_ma100_one_month.pdf", width=16, height=3)
            ggplot() +
            geom_point(data=j2, aes(x=submission_time, y=waiting_time, color="Jobs' waiting times")) +
            geom_line(data=l2, aes(x=date, y=ma100_liquid_load_horizon, color="Liquid load horizon (moving average over 100 values)"))
            dev.off()

            pdf("${output_directory}/hist_nb_jobs_in_queue.pdf", width=16, height=12)
            ggplot(l, aes(nb_jobs_in_queue)) + geom_histogram()
            dev.off()

            pdf("${output_directory}/hist_load_in_queue.pdf", width=16, height=12)
            ggplot(l, aes(load_in_queue)) + geom_histogram()
            dev.off()

            pdf("${output_directory}/hist_llh.pdf", width=16, height=12)
            ggplot(l, aes(load_in_queue)) + geom_histogram()
            dev.off()

            EOF
        - Rscript ${output_directory}/plotscript.R
        - ln -s ${output_directory}/liquid.pdf ${agg_dir}/liquid_${workload["name"]}_full.pdf
        - ln -s ${output_directory}/liquid_one_month.pdf ${agg_dir}/liquid_${workload["name"]}_one_month.pdf
        - ln -s ${output_directory}/liquid_ma100.pdf ${agg_dir}/liquid_ma100_${workload["name"]}_full.pdf
        - ln -s ${output_directory}/liquid_ma100_one_month.pdf ${agg_dir}/liquid_ma100_${workload["name"]}_one_month.pdf
        - ln -s ${output_directory}/hist_nb_jobs_in_queue.pdf ${agg_dir}/hist_nb_jobs_in_queue_${workload["name"]}.pdf
        - ln -s ${output_directory}/hist_load_in_queue.pdf ${agg_dir}/hist_load_in_queue_${workload["name"]}.pdf
        - ln -s ${output_directory}/hist_llh.pdf ${agg_dir}/hist_llh_${workload["name"]}.pdf

#############################
# Commands before instances #
#############################

commands_before_instances:
  # Create an empty aggregated directory, removing the previous one if needed
  - rm -rf ${agg_dir}
  - mkdir -p ${agg_dir}

  # Let's generate the platforms
  - mkdir -p ${platforms_dir}
  - |
      #!/bin/bash -ex

      # For each platform size
      psizes=( 100 128 240 163840 )
      for psize in "${psizes[@]}" ; do
        fname="${platforms_dir}/homo${psize}.xml"
        # If the file does not exist, let's create it
        if [ ! -f fname ]; then
          ${platform_generator_script} --nb ${psize} -o ${fname}
        fi
      done

  # Let's generate the workloads
  - mkdir -p ${workloads_dir}
  - |
      #!/bin/bash -ex

      declare -A urls
      urls["kth_sp2"]="http://www.cs.huji.ac.il/labs/parallel/workload/l_kth_sp2/KTH-SP2-1996-2.1-cln.swf.gz"
      urls["sdsc_sp2"]="http://www.cs.huji.ac.il/labs/parallel/workload/l_sdsc_sp2/SDSC-SP2-1998-4.2-cln.swf.gz"
      urls["hpc2n"]="http://www.cs.huji.ac.il/labs/parallel/workload/l_hpc2n/HPC2N-2002-2.2-cln.swf.gz"
      urls["intrepid"]="http://www.cs.huji.ac.il/labs/parallel/workload/l_anl_int/ANL-Intrepid-2009-1.swf.gz"

      declare -A sizes
      sizes["kth_sp2"]=100
      sizes["sdsc_sp2"]=128
      sizes["hpc2n"]=240
      sizes["intrepid"]=163840

      for wload in "${!urls[@]}" ; do
          echo "${wload}"
          dl_fname="${workloads_dir}/${wload}_downloaded.swf.gz"
          unzip_fname="${workloads_dir}/${wload}_downloaded.swf"
          swf_fname="${workloads_dir}/${wload}.swf"
          json_fname="${workloads_dir}/${wload}.json"

          if [ ! -f "${dl_fname}" ] ; then
              url="${urls[$wload]}"
              wget "${url}" -O "${dl_fname}"
          fi

          if [ ! -f "${unzip_fname}" ] ; then
              gunzip "${dl_fname}"
          fi

          if [ ! -f "${swf_fname}" ] ; then
              mv "${unzip_fname}" "${swf_fname}"
          fi

          if [ ! -f ${json_fname} ] ; then
              "${workload_generator_script}" -t -gwo -i 1 -pf "${sizes[${wload}]}" "${swf_fname}" "${json_fname}" -cs 100e6
          fi
      done

  # Crashes if there is no redis-server running
  - |
      #!/bin/bash -ex

      # Let's make sure a Redis server is running
      redis_running=$(ps faux | grep redis-server | grep -v grep | wc -l)
      if test ${redis_running} -eq 0
      then
          echo "There is no Redis server currently running!"
          exit 1
      fi
