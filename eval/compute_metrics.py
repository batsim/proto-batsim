#!/usr/bin/python3

import argparse
import csv
from pandas import *

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def waiting_time(row):
    return row['starting_time'] - row['submission_time']

def execution_time(row):
    return row['finish_time'] - row['starting_time']

def turnaround_time(row):
    return row['waiting_time'] + row['execution_time']

def slowdown(row):
    return row['turnaround_time'] / row['execution_time']

def bounded_slowdown(row, bound):
    return max(1, row['turnaround_time']/max(row['execution_time'], bound))

def compute_jobs_metrics(batsim_jobs_csv_filename,
                         batsim_schedule_csv_filename,
                         output_metrics_csv_filename,
                         headers_to_add,
                         values_to_add,
                         bounded_stretch_bound):
    jobs = read_csv(batsim_jobs_csv_filename)
    schedule = read_csv(batsim_schedule_csv_filename)

    # Let's compute some metrics for each job
    jobs['waiting_time'] = jobs.apply(lambda row: waiting_time(row),axis=1)
    jobs['execution_time'] = jobs.apply(lambda row: execution_time(row), axis=1)
    jobs['turnaround_time'] = jobs.apply(lambda row:turnaround_time(row),axis=1)
    jobs['slowdown'] = jobs.apply(lambda row: slowdown(row), axis=1)
    jobs['bounded_slowdown'] = jobs.apply(lambda row: bounded_slowdown(row,
        bounded_stretch_bound), axis=1)

    # Let's compute the mean and max of each column
    jobs_means = jobs.mean()
    jobs_max = jobs.max()

    # Let's remove columns whose the mean has no meaning
    del jobs_means['jobID']
    del jobs_means['submission_time']
    del jobs_means['requested_number_of_processors']
    del jobs_means['requested_time']
    del jobs_means['starting_time']
    del jobs_means['execution_time']
    del jobs_means['finish_time']
    del jobs_means['stretch']
    del jobs_means['consumed_energy']

    # Let's rename columns
    jobs_means =jobs_means.rename({'success':'mean_success',
                                   'waiting_time':'mean_waiting_time',
                                   'turnaround_time':'mean_turnaround_time',
                                   'slowdown':'mean_slowdown',
                                   'bounded_slowdown':'mean_bounded_slowdown'})

    # Let's create a dataframe from the series
    output_df = DataFrame(jobs_means)
    output_df = output_df.transpose()

    # Let's add some values in the dataframe
    output_df['makespan'] = jobs_max['finish_time']
    output_df['scheduling_time'] = schedule['scheduling_time']
    output_df['consumed_joules'] = schedule['consumed_joules']

    # Let's add custom columns/values
    if headers_to_add != '' or values_to_add != '':
        col_names = headers_to_add.split(',')
        col_names = [x.strip() for x in col_names]

        values = values_to_add.split(',')
        values = [x.strip() for x in values]

        assert(len(col_names) == len(values)), "Invalid headers/values parameters"

        # Let's quote the strings
        for i in range(len(values)):
            val = values[i]
            if is_number(val):
                values[i] = float(val)
            else:
                values[i] = '"{val}"'.format(val = val)

        for (col_name, value) in zip(col_names, values):
            output_df[col_name] = value

    # Let's write the dataframe into a CSV output file
    output_df.to_csv(output_metrics_csv_filename,
                         index=False,
                         quoting=csv.QUOTE_NONE)

def main():
    # Program parameters parsing
    p = argparse.ArgumentParser(description = 'Computes the scheduling metrics '
                                              'of one schedule')

    p.add_argument('-j', '--batsim_jobs_csv',
                   type = str,
                   required = True,
                   help = 'The name of the file outputted by Batsim which '
                          'contains information about each job')

    p.add_argument('-s', '--batsim_schedule_csv',
                   type = str,
                   required = True,
                   help = 'The name of the file outputted by Batsim which '
                          'contains information about the schedule')

    p.add_argument('-o', '--output_metrics_csv',
                   type = str,
                   required = True,
                   help = 'The name of the output file of this script which '
                          'will contain scheduling metrics')

    p.add_argument('--bounded_stretch_bound',
                   type = float,
                   default = 0,
                   help = 'The bound to use to compute the bounded slowdown '
                          'of each job')

    p.add_argument('--headers_to_add',
                   type = str,
                   default = '',
                   help = 'Defines which headers should be added in the output '
                          'CSV file')

    p.add_argument('--values_to_add',
                   type = str,
                   default = '',
                   help = 'Defines which values should be added in the output '
                          'CSV file')

    args = p.parse_args()

    compute_jobs_metrics(batsim_jobs_csv_filename = args.batsim_jobs_csv,
                         batsim_schedule_csv_filename = args.batsim_schedule_csv,
                         output_metrics_csv_filename = args.output_metrics_csv,
                         headers_to_add = args.headers_to_add,
                         values_to_add = args.values_to_add,
                         bounded_stretch_bound = args.bounded_stretch_bound)

if __name__ == '__main__':
    main()
