#!/usr/bin/python3

import argparse

def generate_options_file(output_filename,
                          power_compute,
                          power_idle,
                          power_sleep,
                          pstate_compute,
                          pstate_sleep,
                          time_switch_on,
                          time_switch_off,
                          energy_switch_on,
                          energy_switch_off,
                          tolerated_slowdown_loss_ratio,
                          indent = 4):
    indent_str = ""
    for i in range(indent):
        indent_str += ' '

    # Let's create a map to avoid copypastes.
    variables = {}
    variables['power_compute'] = power_compute
    variables['power_idle'] = power_idle
    variables['power_sleep'] = power_sleep
    variables['pstate_compute'] = pstate_compute
    variables['pstate_sleep'] = pstate_sleep
    variables['time_switch_on'] = time_switch_on
    variables['time_switch_off'] = time_switch_off
    variables['energy_switch_on'] = energy_switch_on
    variables['energy_switch_off'] = energy_switch_off
    variables['tolerated_slowdown_loss_ratio'] = tolerated_slowdown_loss_ratio

    text_to_write = "{\n"
    i = 1

    for var_name in variables:
        var_value = variables[var_name]
        end = ','

        if i == len(variables):
            end = ''

        text_to_write += '{i}"{var}":{value}{end}\n'.format(i = indent_str,
                                                        var = var_name,
                                                        value = var_value,
                                                        end = end)
        i += 1

    text_to_write += "}\n"

    f = open(output_filename, 'w')
    f.write(text_to_write)
    f.close()

def main():
    # Program parameters parsing
    p = argparse.ArgumentParser(description = 'This script generates input '
                                'files for the energy_bf_dicho scheduling '
                                'variant')

    p.add_argument('output_filename',
                   type = str,
                   help = 'The name of the output JSON file which contains '
                          'the parameters of the energy_bf_dicho scheduling '
                          'variant')

    p.add_argument("--power_compute",
                   type = float,
                   default = 190.738,
                   help = "Sets the power_compute parameter value")

    p.add_argument("--power_idle",
                   type = float,
                   default = 95,
                   help = "Sets the power_idle parameter value")

    p.add_argument("--power_sleep",
                   type = float,
                   default = 9.75,
                   help = "Sets the power_sleep parameter value")

    p.add_argument("--pstate_compute",
                   type = float,
                   default = 0,
                   help = "Sets the pstate_compute parameter value")

    p.add_argument("--pstate_sleep",
                   type = float,
                   default = 13,
                   help = "Sets the pstate_sleep parameter value")

    p.add_argument("--time_switch_on",
                   type = float,
                   default = 152,
                   help = "Sets the time_switch_on parameter value")

    p.add_argument("--time_switch_off",
                   type = float,
                   default = 6.1,
                   help = "Sets the time_switch_off parameter value")

    p.add_argument("--energy_switch_on",
                   type = float,
                   default = 19030,
                   help = "Sets the energy_switch_on parameter value")

    p.add_argument("--energy_switch_off",
                   type = float,
                   default = 620,
                   help = "Sets the energy_switch_off parameter value")

    p.add_argument("--tolerated_slowdown_loss_ratio",
                   type = float,
                   default = 1,
                   help = "Sets the tolerated_slowdown_loss_ratio parameter value")

    args = p.parse_args()

    generate_options_file(
        output_filename = args.output_filename,
        power_compute = args.power_compute,
        power_idle = args.power_idle,
        power_sleep = args.power_sleep,
        pstate_compute = args.pstate_compute,
        pstate_sleep = args.pstate_sleep,
        time_switch_on = args.time_switch_on,
        time_switch_off = args.time_switch_off,
        energy_switch_on = args.energy_switch_on,
        energy_switch_off = args.energy_switch_off,
        tolerated_slowdown_loss_ratio = args.tolerated_slowdown_loss_ratio)

if __name__ == '__main__':
    main()
