#!/usr/bin/python3

import argparse
from pandas import *
from ggplot import *
import os

def create_dir_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def analyse_aggregated(agg_csv_filename,
                       graph_output_dir):
    create_dir_if_not_exists(graph_output_dir)
    agg = read_csv(agg_csv_filename)

    # Metrics to metrics, with another metric as a color
    tuples = [('consumed_joules', 'mean_slowdown', 'algo'),
             ('mean_waiting_time', 'makespan', 'algo'),
             ('mean_turnaround_time', 'makespan', 'algo'),
             ('mean_slowdown', 'makespan', 'algo')]

    for (col_y, col_x, col_c) in tuples:
        p = ggplot(agg, aes(x=col_x, y=col_y, color=col_c)) +\
            geom_point() +\
            facet_wrap('platform_size')
        p.save('{dir}/{x}__to__{y}__{c}.pdf'.format(dir = graph_output_dir,
                                                    x = col_x,
                                                    y = col_y,
                                                    c = col_c))

def main():
    # Program parameters parsing
    p = argparse.ArgumentParser(description = 'Computes the scheduling metrics '
                                              'of one schedule')

    p.add_argument('agg_csv',
                   type = str,
                   help = 'The name of the aggregated CSV file to analyse')

    p.add_argument('graph_output_dir',
                   type = str,
                   help = 'The name of the directory into which the graphs '
                          'will be put')

    args = p.parse_args()

    analyse_aggregated(agg_csv_filename = args.agg_csv,
                       graph_output_dir = args.graph_output_dir)

if __name__ == '__main__':
    main()
