#!/usr/bin/python3

import argparse
import os
import sys
from run_instance import cpp_sched_resource_selection_policies, cpp_sched_algorithms, cpp_sched_queue_orders, batsim_verbosity_levels, run_instance

def main():
    # Arguments parsing
    parser = argparse.ArgumentParser(description='Allows to execute Batsim with the C++ scheduler', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--batsim_filepath', type=str, required=True, help='The Batsim binary filepath')
    parser.add_argument('-p', '--platform', type=str, required=True, help='The platform used by Batsim')
    parser.add_argument('-w', '--workload', type=str, required=True, help='The workload executed in this instance')
    parser.add_argument('--batsim_verbosity', type=str, default='information', help='The verbosity of Batsim. Available values are [{}]'.format(', '.join(batsim_verbosity_levels)))
    parser.add_argument('-m', '--batsim_master_host', type=str, default='master_host', help='The Batsim master host on which all resource management processes are executed')
    parser.add_argument('--batsim_disable_schedule_tracing', action='store_true', help="If set, disables Batsim's schedule tracing")
    parser.add_argument('--enable_energy', action='store_true', help="If set, enables Batsim's energy plugin")
    parser.add_argument('--batsim_allow_space_sharing', action='store_true', help="If set, enables Batsim's resource sharing")

    parser.add_argument('--cpp_scheduler_filepath', type=str, required=True, help='The C++ scheduler binary filepath')
    parser.add_argument('--variants_options', type=str, default='{}', help="The C++ scheduler variants options. Must be a JSON object.")
    parser.add_argument('--variants_options_filepath', type=str, default='', help="The C++ variants options filepath. If set, overrides the variant_options parameter with the content of the given filepath")

    parser.add_argument('-v', '--verbose', action='store_true', help='If set to true, this program will be more verbose')
    parser.add_argument('--out_directory', type=str, default='out', help="The directory into which the results are put")
    parser.add_argument('--scheduling_algorithms_lambda', type=str, default="algo != 'energy_bf'", help="This lambda expression is used to select which scheduling algorithms should be used. The available scheduling algorithms are [{}]".format(', '.join(cpp_sched_algorithms)))
    parser.add_argument('--resource_selection_policies_lambda', type=str, default="policy != ''", help="This lambda expression is used to select which resource selection policies should be used. The available resource selection policies are [{}]".format(', '.join(cpp_sched_resource_selection_policies)))
    parser.add_argument('--queue_orders_lambda', type=str, default="order in ['fcfs', 'desc_slowdown']", help="This lambda expression is used to select which queue orders should be used. The available queue orders are [{}]".format(', '.join(cpp_sched_queue_orders)))
    parser.add_argument('--batsim_socket_timeout_seconds', type=float, default=10, help='The amount of time which is, after Batsim launch, waited to detect the socket creation')

    args = parser.parse_args()

    batsim_disable_schedule_tracing = False
    batsim_enable_energy = False
    batsim_allow_space_sharing = False
    verbose = False

    if args.batsim_disable_schedule_tracing:
        batsim_disable_schedule_tracing = True

    if args.enable_energy:
        batsim_enable_energy = True

    if args.batsim_allow_space_sharing:
        batsim_allow_space_sharing = True

    if args.verbose:
        verbose = True

    # Generating output directories
    out_directory = args.out_directory
    batsim_out_directory = args.out_directory + '/batsim_output'

    if not os.path.isdir(out_directory):
        os.makedirs(out_directory)

    if not os.path.isdir(batsim_out_directory):
        os.makedirs(batsim_out_directory)

    # Executing all instances
    executed_sched_algorithms = list(filter(lambda algo: eval(args.scheduling_algorithms_lambda), cpp_sched_algorithms))
    executed_selection_policies = list(filter(lambda policy: eval(args.resource_selection_policies_lambda), cpp_sched_resource_selection_policies))
    executed_queue_orders = list(filter(lambda order: eval(args.queue_orders_lambda), cpp_sched_queue_orders))

    nb_instances = len(executed_sched_algorithms) * len(executed_selection_policies) * len(executed_queue_orders)
    instance_id = 1
    nb_success = 0
    for algorithm in executed_sched_algorithms:
        for selection_policy in executed_selection_policies:
            for queue_order in executed_queue_orders:
                instance_name = '-'.join([algorithm, selection_policy, queue_order])
                output_prefix = out_directory + '/' + instance_name
                batsim_output_prefix = batsim_out_directory + '/' + instance_name

                print("Running instance {}/{} ({})...".format(instance_id, nb_instances, instance_name), end=' ')
                sys.stdout.flush()
                success = run_instance(batsim_filepath = args.batsim_filepath,
                                       #socket= args.socket,
                                       platform = args.platform,
                                       workload = args.workload,
                                       batsim_verbosity = args.batsim_verbosity,
                                       batsim_master_host = args.batsim_master_host,
                                       batsim_enable_energy = batsim_enable_energy,
                                       batsim_allow_space_sharing = batsim_allow_space_sharing,
                                       batsim_disable_schedule_tracing = batsim_disable_schedule_tracing,
                                       batsim_export_prefix = batsim_output_prefix,
                                       cpp_scheduler_filepath = args.cpp_scheduler_filepath,
                                       resource_selection_policy = selection_policy,
                                       scheduling_algorithm = algorithm,
                                       queue_order = queue_order,
                                       variants_options = args.variants_options,
                                       variants_options_filepath = args.variants_options_filepath,
                                       verbose = verbose,
                                       output_prefix = output_prefix,
                                       batsim_socket_timeout_seconds = args.batsim_socket_timeout_seconds)
                instance_id += 1
                if success:
                    print('OK')
                    nb_success += 1
                else:
                    print('FAIL')

    if nb_instances > 0:
        print('{} instances have been executed. {} succeeded ({} %)'.format(nb_instances, nb_success, (100*nb_success)/nb_instances))
    else:
        print('Nothing to do')

if __name__ == "__main__":
    main()
