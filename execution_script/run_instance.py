#!/usr/bin/python3

import subprocess
import time, argparse

batsim_verbosity_levels = ['quiet', 'network-only', 'information', 'debug']
cpp_sched_resource_selection_policies = ['basic', 'contiguous']
cpp_sched_algorithms = ['conservative_bf', 'easy_bf', 'energy_bf', 'filler']
cpp_sched_queue_orders = ['asc_size', 'asc_walltime', 'desc_bounded_slowdown', 'desc_slowdown', 'desc_walltime', 'fcfs', 'lcfs']

def socket_in_use(sock):
    return sock in open('/proc/net/unix').read()

def wait_for_batsim_to_open_connection(sock, timeout=10, seconds_to_sleep=0.1):
    while timeout > 0 and not socket_in_use(sock):
        time.sleep(seconds_to_sleep)
        timeout -= seconds_to_sleep
    return timeout > 0

def prepare_batsim_cl(batsim_binary_filepath,
                      platform_filepath,
                      workload_filepath,
                      sock = '/tmp/bat_socket',
                      verbosity = 'information',
                      master_hostname = 'master_host',
                      disable_schedule_tracing = False,
                      enable_energy = False,
                      allow_space_sharing = False,
                      export_prefix = 'out'):
    # Input check
    assert (verbosity in batsim_verbosity_levels), "Unknown batsim verbosity level '{}'".format(verbosity)

    # Command line generation
    batsim_cl = [batsim_binary_filepath]

    batsim_cl.append('--socket')
    batsim_cl.append(sock)

    batsim_cl.append('--verbosity')
    batsim_cl.append(verbosity)

    batsim_cl.append('--master-host')
    batsim_cl.append(master_hostname)

    if disable_schedule_tracing:
        batsim_cl.append('--disable-schedule-tracing')

    if enable_energy:
        batsim_cl.append('--energy-plugin')

    if allow_space_sharing:
        batsim_cl.append('--allow-space-sharing')

    batsim_cl.append('--export')
    batsim_cl.append(export_prefix)

    batsim_cl.append(platform_filepath)
    batsim_cl.append(workload_filepath)

    return batsim_cl

def prepare_cpp_sched_cl(cpp_sched_filepath,
                         workload_filepath,
                         sock = '/tmp/bat_socket',
                         resource_selection_policy = 'basic',
                         scheduling_algorithm = 'filler',
                         queue_order = 'fcfs',
                         variants_options = '{}',
                         variants_options_filepath = ''):
    # Input check
    assert (resource_selection_policy in cpp_sched_resource_selection_policies), "Unknown resource selection policy '{}'".format(resource_selection_policy)
    assert(scheduling_algorithm in cpp_sched_algorithms), "Unknown scheduling algorithm '{}'".format(scheduling_algorithm)
    assert(queue_order in cpp_sched_queue_orders), "Unknown queue order '{}'".format(queue_order)

    # Command line generation
    sched_cl = [cpp_sched_filepath]

    sched_cl.append('--json_file')
    sched_cl.append(workload_filepath)

    sched_cl.append('--socket')
    sched_cl.append(sock)

    sched_cl.append('--policy')
    sched_cl.append(resource_selection_policy)

    sched_cl.append('--variant')
    sched_cl.append(scheduling_algorithm)

    sched_cl.append('--queue_order')
    sched_cl.append(queue_order)

    sched_cl.append('--variant_options')
    sched_cl.append(variants_options)

    sched_cl.append('--variant_options_filepath')
    sched_cl.append(variants_options_filepath)

    return sched_cl

def run_instance(batsim_filepath, cpp_scheduler_filepath, platform, workload, socket='/tmp/bat_socket',
                 batsim_verbosity='information', batsim_master_host='master_host', batsim_enable_energy=False,
                 batsim_allow_space_sharing=False, batsim_disable_schedule_tracing=False, batsim_export_prefix='out',
                 resource_selection_policy='basic', scheduling_algorithm='filler', queue_order='fcfs', variants_options = '{}',
                 variants_options_filepath = '', verbose=False, output_prefix='instance', batsim_socket_timeout_seconds=10):
    # Command lines generation
    batsim_cl = prepare_batsim_cl(batsim_binary_filepath = batsim_filepath,
                                  platform_filepath = platform,
                                  workload_filepath = workload,
                                  sock = socket,
                                  verbosity = batsim_verbosity,
                                  master_hostname = batsim_master_host,
                                  disable_schedule_tracing = batsim_disable_schedule_tracing,
                                  enable_energy = batsim_enable_energy,
                                  allow_space_sharing = batsim_allow_space_sharing,
                                  export_prefix = batsim_export_prefix)
    batsim_stdout_file = open(output_prefix + '_batsim.stdout', "w")
    batsim_stderr_file = open(output_prefix + '_batsim.stderr', "w")

    sched_cl = prepare_cpp_sched_cl(cpp_sched_filepath = cpp_scheduler_filepath,
                                    workload_filepath = workload,
                                    sock = socket,
                                    resource_selection_policy = resource_selection_policy,
                                    scheduling_algorithm = scheduling_algorithm,
                                    queue_order = queue_order,
                                    variants_options = variants_options,
                                    variants_options_filepath = variants_options_filepath)
    sched_stdout_file = open(output_prefix + '_sched.stdout', "w")
    sched_stderr_file = open(output_prefix + '_sched.stderr', "w")

    if verbose:
        print('Starting Batsim')
        print(' '.join(batsim_cl + [">", str(batsim_stdout_file.name), "2>", str(batsim_stderr_file.name)]))
    batsim_exec = subprocess.Popen(batsim_cl, stdout=batsim_stdout_file, stderr=batsim_stderr_file, shell=False)

    if verbose:
        print('Waiting for Batsim socket creation')
    success = wait_for_batsim_to_open_connection(socket, batsim_socket_timeout_seconds)
    assert (success), "Timeout: could not reach Batsim socket creation"

    if verbose:
        print('Starting scheduler')
        print(' '.join(sched_cl + [">", str(sched_stdout_file.name), "2>", str(sched_stderr_file.name)]))
    sched_exec = subprocess.Popen(sched_cl, stdout=sched_stdout_file, stderr=sched_stderr_file, shell=False)

    if verbose:
        print('Waiting for the scheduler completion')
    sched_exec.wait()
    if verbose:
        print('Scheduler completed with return code {}'.format(sched_exec.returncode))

    if verbose:
        print('Waiting for Batsim completion')
    batsim_exec.wait()
    if verbose:
        print('Batsim completed with return code {}'.format(batsim_exec.returncode))

    return (batsim_exec.returncode == 0) and (sched_exec.returncode == 0)

def main():
    parser = argparse.ArgumentParser(description='Allows to execute Batsim with the C++ scheduler')
    parser.add_argument('--batsim_filepath', type=str, required=True, help='The Batsim binary filepath')
    parser.add_argument('-s', '--socket', type=str, default='/tmp/bat_socket', help='The socket used to communicate between Batsim and the scheduler')
    parser.add_argument('-p', '--platform', type=str, required=True, help='The platform used by Batsim')
    parser.add_argument('-w', '--workload', type=str, required=True, help='The workload executed in this instance')
    parser.add_argument('--batsim_verbosity', type=str, default='information', help='The verbosity of Batsim. Available values are [{}]'.format(', '.join(batsim_verbosity_levels)))
    parser.add_argument('-m', '--batsim_master_host', type=str, default='master_host', help='The Batsim master host on which all resource management processes are executed')
    parser.add_argument('--batsim_disable_schedule_tracing', action='store_true', help="If set, disables Batsim's schedule tracing")
    parser.add_argument('--enable_energy', action='store_true', help="If set, enables Batsim's energy plugin")
    parser.add_argument('--batsim_allow_space_sharing', action='store_true', help="If set, enables Batsim's resource sharing")
    parser.add_argument('--batsim_export_prefix', type=str, default='out', help="The Batsim export prefix")

    parser.add_argument('--cpp_scheduler_filepath', type=str, required=True, help='The C++ scheduler binary filepath')
    parser.add_argument('--resource_selection_policy', type=str, default='basic', help="The C++ scheduler resource selection policy. Available values are [{}]".format(', '.join(cpp_sched_resource_selection_policies)))
    parser.add_argument('--scheduling_algorithm', type=str, default='filler', help="The C++ scheduler scheduling algorithm. Available values are [{}]".format(', '.join(cpp_sched_algorithms)))
    parser.add_argument('--queue_order', type=str, default='fcfs', help="The C++ scheduler queue order. Available values are [{}]".format(', '.join(cpp_sched_queue_orders)))
    parser.add_argument('--variants_options', type=str, default='{}', help="The C++ scheduler variants options. Must be a JSON object.")
    parser.add_argument('--variants_options_filepath', type=str, default='', help="The C++ variants options filepath. If set, overrides the variant_options parameter with the content of the given filepath")

    parser.add_argument('-v', '--verbose', action='store_true', help='If set to true, this program will be more verbose')
    parser.add_argument('--output_prefix', type=str, default='instance', help="The output prefix of files generated by this program")
    parser.add_argument('--batsim_socket_timeout_seconds', type=float, default=10, help='The amount of time which is, after Batsim launch, waited to detect the socket creation')

    args = parser.parse_args()

    batsim_disable_schedule_tracing = False
    batsim_enable_energy = False
    batsim_allow_space_sharing = False
    verbose = False

    if args.batsim_disable_schedule_tracing:
        batsim_disable_schedule_tracing = True

    if args.enable_energy:
        batsim_enable_energy = True

    if args.batsim_allow_space_sharing:
        batsim_allow_space_sharing = True

    if args.verbose:
        verbose = True

    return run_instance(batsim_filepath = args.batsim_filepath,
                        socket= args.socket,
                        platform = args.platform,
                        workload = args.workload,
                        batsim_verbosity = args.batsim_verbosity,
                        batsim_master_host = args.batsim_master_host,
                        batsim_enable_energy = batsim_enable_energy,
                        batsim_allow_space_sharing = batsim_allow_space_sharing,
                        batsim_disable_schedule_tracing = batsim_disable_schedule_tracing,
                        batsim_export_prefix = args.batsim_export_prefix,
                        cpp_scheduler_filepath = args.cpp_scheduler_filepath,
                        resource_selection_policy = args.resource_selection_policy,
                        scheduling_algorithm = args.scheduling_algorithm,
                        queue_order = args.queue_order,
                        variants_options = args.variants_options,
                        variants_options_filepath = args.variants_options_filepath,
                        verbose = verbose,
                        output_prefix = args.output_prefix,
                        batsim_socket_timeout_seconds = args.batsim_socket_timeout_seconds)

if __name__ == "__main__":
    main()
